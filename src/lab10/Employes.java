/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-04
 */
package lab10;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author johnny
 *
 */
public class Employes {

	final String SEPERATEUR_CHAMPS = ":";
	final int NBCHANPS_SI_HEURES_SUPPL = 3;
	//final int NBCHANPS_MINIMUM = 2;
	public static void main(String[] args) {
       Employes emp = new Employes();
       emp.lireDonnees();
	}
   
	void lireDonnees(){
		Path fichier = Paths.get("donnees/info.txt");
		try(BufferedReader reader = Files.newBufferedReader(fichier);){
		     String ligne=reader.readLine();//lecture 1ere ligne
		     while(ligne!=null){
		    	  ligne=ligne.trim();//enlever les espaces vides eventuels
		    	 String[] elems=ligne.split(this.SEPERATEUR_CHAMPS);
		    	 String nom = elems[0];
		    	 System.out.format("--------------------%n");//ligne pour lisibilité affichage
		    	 System.out.format("Nom : %s%n", nom);
		    	 double hnormal = Double.parseDouble(elems[1]);//convertir chaine en double
		    	 System.out.format("HNormales : %.2f%n", hnormal);
		    	 double hsupp = 0.0;
		    	 if(elems.length>=this.NBCHANPS_SI_HEURES_SUPPL){
		    		 //s'il y a des heures supplementaires
		    		 hsupp = Double.parseDouble(elems[2]);//conversion chaine vers reel
		    		 System.out.format("HSupp : %.2f%n", hsupp);
		    	 }
		    	 ligne=reader.readLine();//ligne suivante
		    	 	     }
			}catch (IOException x){
			   System.out.println("Erreur lecture fichier");
			}
	}
	
}
